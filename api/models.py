from django.db import models


class Note(models.Model):
    title = models.TextField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)

