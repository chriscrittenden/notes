import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import produce from 'immer';
import {Route, Link} from 'react-router-dom';
import axios from 'axios';

class App extends Component {
    state = {
        notes: [],
        drawer_open: false
    };

    componentDidMount() {
        axios.get("/notes/").then(res => {
            this.setState({
                notes: res.data
            });
        });
    }

    handleAddNote = () => {
        axios.post("/notes/", {title: "New Note", text: ""}).then(res => {
            this.setState({
                notes: produce(this.state.notes, draft => {draft.push(res.data)})
            }, () => {
                this.props.history.push("/notes/" + res.data.id + "/");
            });
        });
    };

    handleDeleteNote = (note_id) => {
        this.props.history.push("/");

        axios.delete("/notes/" + note_id + "/").then(res => {
            const index = this.state.notes.findIndex(x => x.id === note_id);
            this.setState({
                notes: produce(this.state.notes, draft => {draft.splice(index, 1)})
            })
        })
    };

    handleEditNoteTitle = (note_id, value) => {
        const index = this.state.notes.findIndex(x => x.id === note_id);
        this.setState({
            notes: produce(this.state.notes, draft => {draft[index].title = value})
        })
    };

    handleEditNoteText = (note_id, value) => {
        const index = this.state.notes.findIndex(x => x.id === note_id);
        this.setState({
            notes: produce(this.state.notes, draft => {draft[index].text = value})
        })
    };

    handleSaveNote = (note_id) => {
        const index = this.state.notes.findIndex(x => x.id === note_id);

        axios.put("/notes/" + note_id + "/", this.state.notes[index]).then(res => {
            console.log("success");
        })
    };

    render() {
        return (
            <div>
                {/* The AppBar */}
                <AppBar position="static">
                    <Toolbar>
                        <IconButton style={{marginLeft: -12, marginRight: 20}} color="inherit" onClick={() => this.setState({drawer_open: true})}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" style={{flexGrow: 1}}>chrisnote</Typography>
                        <Button color="inherit"onClick={this.handleAddNote}>New Note</Button>
                    </Toolbar>
                </AppBar>

                {/* The Drawer */}
                <Drawer open={this.state.drawer_open} onClose={() => this.setState({drawer_open: false})}>
                    {this.state.notes.map(note => {
                        return (
                            <ListItem key={note.id} button component={Link} to={'/notes/' + note.id} onClick={() => this.setState({drawer_open: false})}>
                                <ListItemText primary={note.title} secondary={note.text} />
                            </ListItem>
                        )
                    })}
                </Drawer>

                {/* The List */}
                <Route path={'/notes/:note_id'} render={routeProps => {
                    const note = this.state.notes.length > 0 ? this.state.notes.find(x => x.id === parseInt(routeProps.match.params.note_id, 10)) : {title: "", text: ""};
                    return (
                        <Note {...routeProps} note={note} onDeleteNote={this.handleDeleteNote} onEditNoteTitle={this.handleEditNoteTitle} onEditNoteText={this.handleEditNoteText} onSaveNote={this.handleSaveNote}/>
                    )
                }}/>
            </div>
        );
    }
}

class Note extends Component {
    render () {
        const note_id = parseInt(this.props.match.params.note_id, 10);

        return (
            <div style={{display: 'flex', justifyContent: 'center'}}>
                <Paper style={{maxWidth: '1020px', width: '100%', marginTop: '40px', padding: 40}}>
                    <TextField style={{marginBottom: 40}} placeholder={"Note Title"} fullWidth value={this.props.note.title} onChange={(e) => this.props.onEditNoteTitle(note_id, e.target.value)}/>
                    <TextField style={{marginBottom: 40}} placeholder={"Note Text"} fullWidth value={this.props.note.text} onChange={(e) => this.props.onEditNoteText(note_id, e.target.value)} multiline/>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Button onClick={() => this.props.onDeleteNote(note_id)} variant="raised">Delete</Button>
                        <Button onClick={() => this.props.onSaveNote(note_id)} variant="raised" color="secondary">Save</Button>
                    </div>
                </Paper>
            </div>
        )
    }
}

export default App;